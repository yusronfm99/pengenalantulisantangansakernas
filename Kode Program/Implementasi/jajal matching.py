import cv2
import numpy as np
import pytesseract
import os

# C:\Program Files\Tesseract-OCR

per = 25
pixelThreshold=500

roi =  [[(577, 244), (2246, 308), 'huruf', 'namaPengisi'],
        [(577, 327), (2246, 392), 'angka', 'hpPengisi'],
        [(577, 409), (2246, 475), 'campur', 'alamat'],

        [(308, 970), (1089, 1100), 'huruf', 'namaART1'],
        [(1120, 1011), (1167, 1071), 'angka', 'hub11'],
        [(1178, 1012), (1222, 1070), 'angka', 'hub12'],
        [(1280, 1032), (1316, 1051), 'box', 'laki1'],
        [(1529, 1032), (1566, 1051), 'box', 'perempuan1'],
        [(1835, 1024), (1879, 1083), 'angka', 'bul11'],
        [(1892, 1025), (1937, 1084), 'angka', 'bul12'],
        [(1979, 1025), (2024, 1084), 'angka', 'th11'],
        [(2036, 1025), (2080, 1084), 'angka', 'th12'],
        [(2091, 1025), (2137, 1084), 'angka', 'th13'],
        [(2148, 1025), (2194, 1084), 'angka', 'th14'],
        [(2227, 1025), (2273, 1084), 'angka', 'umur11'],
        [(2284, 1025), (2330, 1084), 'angka', 'umur12'],

        [(308, 1113), (1089, 1250), 'huruf', 'namaART2'],
        [(1120, 1157), (1167, 1217), 'angka', 'hub21'],
        [(1178, 1157), (1222, 1217), 'angka', 'hub22'],
        [(1280, 1177), (1316, 1200), 'box', 'laki2'],
        [(1529, 1177), (1566, 1200), 'box', 'perempuan2'],
        [(1835, 1171), (1879, 1233), 'angka', 'bul21'],
        [(1892, 1171), (1937, 1233), 'angka', 'bul22'],
        [(1979, 1171), (2024, 1233), 'angka', 'th21'],
        [(2036, 1171), (2080, 1233), 'angka', 'th22'],
        [(2091, 1171), (2137, 1233), 'angka', 'th23'],
        [(2148, 1171), (2194, 1233), 'angka', 'th24'],
        [(2227, 1171), (2273, 1233), 'angka', 'umur21'],
        [(2284, 1171), (2330, 1233), 'angka', 'umur22'],

        [(308, 1261), (1089, 1397), 'huruf', 'namaART3'],
        [(1120, 1304), (1167, 1363), 'angka', 'hub31'],
        [(1178, 1304), (1222, 1363), 'angka', 'hub32'],
        [(1280, 1324), (1316, 1345), 'box', 'laki3'],
        [(1529, 1324), (1566, 1345), 'box', 'perempuan3'],
        [(1835, 1318), (1879, 1378), 'angka', 'bul31'],
        [(1892, 1318), (1937, 1378), 'angka', 'bul32'],
        [(1979, 1318), (2024, 1378), 'angka', 'th31'],
        [(2036, 1318), (2080, 1378), 'angka', 'th32'],
        [(2091, 1318), (2137, 1378), 'angka', 'th33'],
        [(2148, 1318), (2194, 1378), 'angka', 'th34'],
        [(2227, 1318), (2273, 1378), 'angka', 'umur31'],
        [(2284, 1318), (2330, 1378), 'angka', 'umur32'],

        [(307, 1407), (1088, 1545), 'huruf', 'namaART4'],
        [(1119, 1450), (1166, 1512), 'angka', 'hub41'],
        [(1177, 1450), (1221, 1512), 'angka', 'hub42'],
        [(1279, 1470), (1315, 1493), 'box', 'laki4'],
        [(1528, 1470), (1565, 1493), 'box', 'perempuan4'],
        [(1834, 1465), (1878, 1526), 'angka', 'bul41'],
        [(1891, 1465), (1936, 1526), 'angka', 'bul42'],
        [(1978, 1465), (2023, 1526), 'angka', 'th41'],
        [(2035, 1465), (2079, 1526), 'angka', 'th42'],
        [(2090, 1465), (2136, 1526), 'angka', 'th43'],
        [(2147, 1465), (2193, 1526), 'angka', 'th44'],
        [(2226, 1465), (2272, 1526), 'angka', 'umur41'],
        [(2283, 1465), (2329, 1526), 'angka', 'umur42'],

        [(307, 1556), (1088, 1692), 'huruf', 'namaART5'],
        [(1119, 1600), (1166, 1660), 'angka', 'hub51'],
        [(1177, 1600), (1221, 1660), 'angka', 'hub52'],
        [(1279, 1621), (1315, 1643), 'box', 'laki5'],
        [(1528, 1621), (1565, 1643), 'box', 'perempuan5'],
        [(1834, 1615), (1878, 1675), 'angka', 'bul51'],
        [(1891, 1615), (1936, 1675), 'angka', 'bul52'],
        [(1978, 1615), (2023, 1675), 'angka', 'th51'],
        [(2035, 1615), (2079, 1675), 'angka', 'th52'],
        [(2090, 1615), (2136, 1675), 'angka', 'th53'],
        [(2147, 1615), (2193, 1675), 'angka', 'th54'],
        [(2226, 1615), (2272, 1675), 'angka', 'umur51'],
        [(2283, 1615), (2329, 1675), 'angka', 'umur52'],

        [(307, 1703), (1088, 1839), 'huruf', 'namaART6'],
        [(1119, 1746), (1166, 1807), 'angka', 'hub61'],
        [(1177, 1746), (1221, 1807), 'angka', 'hub62'],
        [(1279, 1767), (1315, 1788), 'box', 'laki6'],
        [(1528, 1767), (1565, 1788), 'box', 'perempuan6'],
        [(1834, 1760), (1878, 1821), 'angka', 'bul61'],
        [(1891, 1760), (1936, 1821), 'angka', 'bul62'],
        [(1978, 1760), (2023, 1821), 'angka', 'th61'],
        [(2035, 1760), (2079, 1821), 'angka', 'th62'],
        [(2090, 1760), (2136, 1821), 'angka', 'th63'],
        [(2147, 1760), (2193, 1821), 'angka', 'th64'],
        [(2226, 1760), (2272, 1821), 'angka', 'umur61'],
        [(2283, 1760), (2328, 1821), 'angka', 'umur62'],

        [(307, 1848), (1088, 1990), 'huruf', 'namaART7'],
        [(1119, 1895), (1166, 1956), 'angka', 'hub71'],
        [(1177, 1895), (1221, 1956), 'angka', 'hub72'],
        [(1279, 1916), (1314, 1939), 'box', 'laki7'],
        [(1527, 1916), (1564, 1939), 'box', 'perempuan7'],
        [(1833, 1910), (1877, 1972), 'angka', 'bul71'],
        [(1890, 1910), (1935, 1972), 'angka', 'bul72'],
        [(1977, 1910), (2022, 1972), 'angka', 'th71'],
        [(2034, 1910), (2078, 1972), 'angka', 'th72'],
        [(2089, 1910), (2136, 1972), 'angka', 'th73'],
        [(2146, 1910), (2192, 1972), 'angka', 'th74'],
        [(2225, 1910), (2271, 1972), 'angka', 'umur71'],
        [(2282, 1910), (2328, 1972), 'angka', 'umur72'],

        [(307, 2000), (1088, 2136), 'huruf', 'namaART8'],
        [(1118, 2044), (1165, 2105), 'angka', 'hub81'],
        [(1176, 2044), (1220, 2105), 'angka', 'hub82'],
        [(1278, 2066), (1314, 2087), 'box', 'laki8'],
        [(1527, 2066), (1564, 2087), 'box', 'perempuan8'],
        [(1833, 2058), (1877, 2119), 'angka', 'bul81'],
        [(1890, 2058), (1935, 2119), 'angka', 'bul82'],
        [(1977, 2058), (2022, 2119), 'angka', 'th81'],
        [(2034, 2058), (2078, 2119), 'angka', 'th82'],
        [(2089, 2058), (2135, 2119), 'angka', 'th83'],
        [(2146, 2058), (2192, 2119), 'angka', 'th84'],
        [(2225, 2058), (2271, 2119), 'angka', 'umur81'],
        [(2282, 2058), (2328, 2119), 'angka', 'umur82'],

        [(307, 2144), (1088, 2283), 'huruf', 'namaART9'],
        [(1118, 2190), (1165, 2251), 'angka', 'hub91'],
        [(1176, 2190), (1220, 2251), 'angka', 'hub92'],
        [(1278, 2211), (1314, 2233), 'box', 'laki9'],
        [(1527, 2211), (1564, 2233), 'box', 'perempuan9'],
        [(1833, 2204), (1877, 2267), 'angka', 'bul91'],
        [(1890, 2204), (1934, 2267), 'angka', 'bul92'],
        [(1977, 2204), (2022, 2267), 'angka', 'th91'],
        [(2034, 2204), (2078, 2267), 'angka', 'th92'],
        [(2089, 2204), (2135, 2267), 'angka', 'th93'],
        [(2146, 2204), (2192, 2267), 'angka', 'th94'],
        [(2225, 2204), (2271, 2267), 'angka', 'umur91'],
        [(2282, 2204), (2328, 2267), 'angka', 'umur92'],

        [(307, 2292), (1088, 2430), 'huruf', 'namaART10'],
        [(1118, 2339), (1165, 2398), 'angka', 'hub101'],
        [(1176, 2339), (1220, 2398), 'angka', 'hub102'],
        [(1278, 2360), (1314, 2381), 'box', 'laki10'],
        [(1527, 2360), (1564, 2381), 'box', 'perempuan10'],
        [(1833, 2352), (1877, 2413), 'angka', 'bul101'],
        [(1890, 2352), (1934, 2413), 'angka', 'bul102'],
        [(1977, 2352), (2022, 2413), 'angka', 'th101'],
        [(2034, 2352), (2078, 2413), 'angka', 'th102'],
        [(2089, 2352), (2135, 2413), 'angka', 'th103'],
        [(2146, 2352), (2192, 2413), 'angka', 'th104'],
        [(2225, 2352), (2271, 2413), 'angka', 'umur101'],
        [(2282, 2352), (2328, 2413), 'angka', 'umur102'],

        [(359, 2908), (420, 2959), 'box', '1y'],
        [(1405, 2911), (1465, 2960), 'box', '1t'],
        [(359, 3036), (419, 3085), 'box', '2y'],
        [(1408, 3045), (1467, 3094), 'box', '2t'],
        [(358, 3155), (418, 3201), 'box', '3y'],
        [(1407, 3162), (1466, 3208), 'box', '3t'],
        [(358, 3294), (417, 3343), 'box', '4y'],
        [(1407, 3300), (1468, 3350), 'box', '4t']]
# roi = [[(143, 590), (200, 636), 'number', 'kodeprov1'],
#        [(200, 590), (256, 640), 'number', 'kodeprov2'],
#        [(273, 593), (326, 640), 'number', 'kodekab1'],
#        [(330, 590), (386, 640), 'number', 'kodekab2'],
#        [(393, 590), (450, 640), 'number', 'kodekec1'],
#        [(450, 593), (506, 640), 'number', 'kodekec2'],
#        [(506, 593), (563, 640), 'number', 'kode kec3'],
#        [(576, 593), (630, 640), 'number', 'kodedes1'],
#        [(633, 593), (686, 640), 'number', 'kodedes2'],
#        [(690, 593), (743, 640), 'number', 'kodedes3'],
#        [(756, 593), (810, 640), 'number', 'klasifikasi'],
#        [(826, 593), (880, 640), 'number', 'nobs1'],
#        [(883, 596), (936, 643), 'number', 'nobs2'],
#        [(940, 593), (990, 640), 'number', 'nobs3'],
#        [(996, 593), (1046, 643), 'letter', 'kodebs'],
#        [(1063, 596), (1116, 643), 'number', 'nosampel1'],
#        [(1120, 596), (1173, 643), 'number', 'nosampel2'],
#        [(1180, 596), (1230, 643), 'number', 'nosampel3'],
#        [(1236, 600), (1286, 646), 'number', 'nosampel4'],
#        [(1290, 600), (1343, 646), 'number', 'nosampel5'],
#        [(1360, 596), (1413, 643), 'number', 'nort1'],
#        [(1416, 596), (1466, 643), 'number', 'nort2'],
#        [(743, 676), (1466, 723), 'text', 'namakrt'],
#        [(300, 743), (343, 793), 'number', 'kodepetugas1'],
#        [(346, 743), (386, 793), 'number', 'kodepetugas2'],
#        [(390, 743), (430, 793), 'number', 'kodepetugas3'],
#        [(623, 743), (1033, 793), 'text', 'namapetugas'],
#        [(1226, 746), (1470, 796), 'numbers', 'nohppetugas']]



pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

imgQ = cv2.imread('22A4.jpg')
h,w,c = imgQ.shape
#imgQ = cv2.resize(imgQ,(w//3,h//3))

orb = cv2.ORB_create(3000)
kp1, des1 = orb.detectAndCompute(imgQ,None)
impKp1 = cv2.drawKeypoints(imgQ,kp1,None)

path = 'sakernasform2'
myPicList = os.listdir(path)
print(myPicList)
for j,y in enumerate(myPicList):
    img = cv2.imread(path +"/"+y)
    img = cv2.resize(img, (w // 3, h // 3))
    #cv2.imshow(y, img)
    kp2, des2 = orb.detectAndCompute(img,None)
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    matches = bf.match(des2,des1)
    matches.sort(key= lambda x: x.distance)
    good = matches[:int(len(matches)*(per/100))]
    imgMatch = cv2.drawMatches(img,kp2,imgQ,kp1,good[:100],None,flags=2)
    
    #cv2.namedWindow("output", cv2.WINDOW_NORMAL)
imgMatch = cv2.resize(imgMatch, (w // 4, h // 6)) 
cv2.imshow(y, imgMatch)
cv2.waitKey(0) 
    
#     srcPoints = np.float32([kp2[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
#     dstPoints = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

#     M, _ = cv2.findHomography(srcPoints,dstPoints,cv2.RANSAC,5.0)
#     imgScan = cv2.warpPerspective(img,M,(w,h), flags=cv2.INTER_CUBIC)
#     #imgScan = cv2.resize(imgScan,(w//3,h//3))
#     #cv2.imshow(y, imgScan)
#     imgShow = imgScan.copy()
#     imgMask = np.zeros_like(imgShow)

#     myData = []

#     print(f'################## Extracting Data from Form {j}  ##################')

#     for x,r in enumerate(roi):

#         cv2.rectangle(imgMask, (r[0][0],r[0][1]),(r[1][0],r[1][1]),(255,255,255),cv2.FILLED)
#         imgShow = cv2.addWeighted(imgShow,0.99,imgMask,0.1,0)

#     imgShow = cv2.resize(imgShow, (w // 5, h // 5))
#     cv2.imshow(y+"2", imgShow)
#     cv2.waitKey(0) 
#         # imgCrop = imgScan[r[0][1]:r[1][1], r[0][0]:r[1][0]]
#         # cv2.imshow(str(x), imgCrop)
#         # cv2.waitKey(0) 
# cv2.imwrite(r'C:\Users\YUSRON\Pictures\CODINGANKU\EMNIST-CHARSEG\COBACOBA\zona isian 2 putih.jpg',imgShow)

#         #save POTONGAN gambare
#         cv2.imwrite(r'C:\Users\YUSRON\Pictures\OCR on Forms and Receipts\OUTPUT\aaa_{}.jpg'.format(x),imgCrop)


        
#         print('{} :{}'.format(r[3],pytesseract.image_to_string(imgCrop)))
#         myData.append(pytesseract.image_to_string(imgCrop))
        
#         #nempelin hasil prediksi
#         cv2.putText(imgShow,str(myData[x]),(r[0][0],r[0][1]),cv2.FONT_HERSHEY_COMPLEX,2.5,(0,0,255),4)

#     with open('Outputjajal.csv','a+') as f:
#         for data in myData:
#             f.write((str(data)+','))
#         f.write('\n')

#     imgShow3 = cv2.resize(imgShow, (w // 3, h // 3))
#     print(myData)
#     cv2.imshow(y+"2", imgShow3)
#     cv2.imwrite(y,imgShow)


# #cv2.imshow("KeyPointsQuery",impKp1)
# imgQ = cv2.resize(imgQ, (w // 3, h // 3))

# cv2.imshow("Output",imgQ)
# cv2.waitKey(0)

            
    